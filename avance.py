import random
import numpy as np
import matplotlib.pyplot as plt
import math
from collections import Counter

def randomi(a,b,c):
    r=random.random()
    #r=random.uniform(0,1)
    if(r<a):
        return 1
    elif (r<a+b):
        return 2
    else:
        return 3

def hfunc(x,y):
    hfunc=[]
    for i in range(1,x+1):
        for j in range(1,y+1):
            hfunc.append((i+j)/20)
    hfunc=np.reshape(hfunc, (x, y))
    return hfunc

def ploteo(matrix,x,y,names=False):
    fig, ax = plt.subplots(figsize=(8,16))
    ax.matshow(matrix, cmap=plt.cm.Blues)
    if (names==True):
        for i in range(x):
            for j in range(y):
                #c = intersection_matrix[j,i]
                c=matrix[j,i]
                ax.text(i, j, str(c), va='center', ha='center')
    plt.show()

def boltzmann(j,k,r,x,g,beta):
    s=0
    if(x[j-1][k]==r):
        s=s+1
    elif(x[j+1][k]==r):
        s=s+1
    elif(x[j][k-1]==r):
        s=s+1
    elif(x[j][k+1]==r):
        s=s+1
        
    return math.exp(beta*(s+g*r))


X=50
Y=50
hfunc=hfunc(X, Y)

#n=np.linspace(0,49, dtype=int)
n2=np.linspace(2,48, dtype=int)
q=3
m=1000
d=100
beta=0.5
r=[1,2,3]
x=np.zeros((50,50))
for ii in range (m):
    a=[]
    b=[]
    j,k=np.random.choice(n2,1)[0],np.random.choice(n2,1)[0]
    #print(j,k)
    for i in r:
        a.append(boltzmann(j,k,i,x,hfunc[j][k],beta))
    a=np.array(a)
    b=a/sum(a)
    l=randomi(b[0],b[1],b[2])
    x[j][k]=l
    #print(x)
    if (ii%d==0):  
        ploteo(x,50,50,False)



